# fuzzy-c-py

Simple implementation for Fuzzy-c-means unsupervised clustering algorithm.
Based on description and nomenclature on : https://sites.google.com/site/dataclusteringalgorithms/fuzzy-c-means-clustering-algorithm

# Dependencies
1. python3
2. numpy

# Installation

Using pipenv

```bash
git checkout git@gitlab.com:Ev3ryDay1sL3gDay/fuzzy-c-py.git
cd fuzzy-c-py
pipenv install
pipenv run python fuzzy-c-means.py
```

