#  Copyright © 2018 Anant Sujatanagarjuna
#  This file is part of fuzzy-c-py.

#     fuzzy-c-py is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     fuzzy-c-py is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with fuzzy-c-py.  If not, see <https://www.gnu.org/licenses/>.

"""
Simple implementation for Fuzzy-c-means unsupervised clustering algorithm.
Based on description and nomenclature on https://sites.google.com/site/dataclusteringalgorithms/fuzzy-c-means-clustering-algorithm
"""

from collections import defaultdict
import numpy as np


def train_fuzzy_c(X, V, B=0.4, distance=lambda x, y: abs(x-y), m=1.1):
    """
    X should be a collection of `n` data points, can be anything.
    V should be a collection of `c` default cluster centroids.
    B represents β, termination criterion, between [0, 1].
    m is the fuzziness index in [1, ∞).
    """
    n = len(X)
    c = len(V)

    U_old = np.zeros((n, c), np.float64)
    U_new = np.array(U_old)

    while True:
        # Compute fuzzy membership matrix
        for i in range(n):
            for j in range(c):
                summ = 0
                for k in range(c):
                    summ += (distance(X[i], V[j]) /
                             distance(X[i], V[k])) ** (2/(m-1))
                U_new[i][j] = 1.0/summ
        # Compute fuzzy centroids based on U
        for j in range(c):
            num = 0
            den = 0
            for i in range(n):
                weight_ij = U_new[i][j] ** m
                num += weight_ij * X[i]
                den += weight_ij
            V[j] = num/den
        improvement = np.linalg.norm(np.subtract(U_new, U_old), ord="fro")
        print()
        if improvement < B:
            break
        U_old = U_new
    J = 0
    for i in range(n):
        for j in range(c):
            J += (U_new[i][j]**m)*distance(X[i], V[j])
    return U_new, V, J


if __name__ == "__main__":
    X = [float(x) for x in open("scores.txt")]
    V = [0, 70]
    B = 0.001
    m = 1.2

    def distance(x, y): return abs(x - y)
    U, V, J = train_fuzzy_c(X, V, distance=distance, B=B, m=m)
    print("Centroids: ", V)
    print("J: ", J)

    # classification = []
    # for k, v in enumerate(X):
